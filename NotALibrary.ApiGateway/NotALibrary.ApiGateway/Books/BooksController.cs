﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using NotALibrary.ApiGateway.Books.Dtos;
using NotALibrary.ApiGateway.Books.Services;
using NotALibrary.ApiGateway.Common;
using NotALibrary.Commands;

namespace NotALibrary.ApiGateway.Books
{
    public class BooksController : BaseController
    {
        private readonly IBus _bus;
        private readonly IMapper _mapper;
        private readonly IBooksService _booksService;

        public BooksController(IBus bus, 
            IMapper mapper,
            IBooksService booksService)
        {
            _bus = bus;
            _mapper = mapper;
            _booksService = booksService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBooks()
            => ResultFromResponse(await _booksService.GetAllAsync());

        [HttpGet("{bookId}")]
        public async Task<IActionResult> GetById([FromRoute] Guid bookId)
            => ResultFromResponse(await _booksService.GetByIdAsync(bookId));

        [HttpPost]
        public Task<IActionResult> AddBook([FromBody] CreateBookDto dto)
            => ProcessCommandAsync(_bus, _mapper.Map<CreateBook>(dto));

        [HttpPost("{bookId}/items")]
        public Task<IActionResult> AddBookItems([FromRoute] Guid bookId, [FromQuery] int quantity = 1)
            => ProcessCommandAsync(_bus, new AddBookItems { BookId = bookId, Quantity = quantity});

        [HttpDelete("{bookId}/items/{itemId}")]
        public Task<IActionResult> RemoveBookItem([FromRoute] Guid bookId, [FromRoute] Guid itemId)
            => ProcessCommandAsync(_bus, new RemoveBookItem { BookId = bookId, ItemId = itemId });

        [HttpPatch("{bookId}/items/{itemId}/checkout")]
        public Task<IActionResult> CheckOutBookItem([FromRoute] Guid bookId, [FromRoute] Guid itemId, [FromBody] CheckOutBookDto dto)
            => ProcessCommandAsync(_bus, _mapper.Map<CheckOutBook>(dto).WithBookId(bookId).WithItemId(itemId));

        [HttpPatch("{bookId}/items/{itemId}/checkin")]
        public Task<IActionResult> CheckInBookItem([FromRoute] Guid bookId, [FromRoute] Guid itemId)
            => ProcessCommandAsync(_bus, new CheckInBook {BookId = bookId, ItemId = itemId});
    }
}
