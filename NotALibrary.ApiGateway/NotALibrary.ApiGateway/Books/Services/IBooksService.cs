﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NotALibrary.ApiGateway.Books.ViewModels.cs;
using RestEase;

namespace NotALibrary.ApiGateway.Books.Services
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized, Body = BodySerializationMethod.Serialized)]
    public interface IBooksService
    {
        [AllowAnyStatusCode]
        [Get("api/Books")]
        Task<Response<IEnumerable<Book>>> GetAllAsync();

        [AllowAnyStatusCode]
        [Get("api/Books/{bookId}")]
        Task<Response<Book>> GetByIdAsync([Path] Guid bookId);
    }
}
