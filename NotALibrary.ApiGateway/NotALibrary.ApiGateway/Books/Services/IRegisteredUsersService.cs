﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NotALibrary.ApiGateway.Books.ViewModels.cs;
using RestEase;

namespace NotALibrary.ApiGateway.Books.Services
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized, Body = BodySerializationMethod.Serialized)]
    public interface IRegisteredUsersService
    {
        [AllowAnyStatusCode]
        [Get("api/RegisteredUsers")]
        Task<Response<IEnumerable<RegisteredUser>>> GetAllAsync();

        [AllowAnyStatusCode]
        [Get("api/RegisteredUsers/{userId}")]
        Task<Response<RegisteredUser>> GetByIdAsync([Path] Guid userId);
    }
}
