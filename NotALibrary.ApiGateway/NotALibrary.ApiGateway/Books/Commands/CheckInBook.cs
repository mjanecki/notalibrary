﻿using System;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Commands
{
    public class CheckInBook : CommandBase
    {
        public Guid BookId { get; set; }
        public Guid ItemId { get; set; }
    }
}
