﻿using System;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Commands
{
    public class AddBookItems : CommandBase
    {
        public Guid BookId { get; set; }
        public int Quantity { get; set; }

    }
}