﻿using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Commands
{
    public class RegisterUser : CommandBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
