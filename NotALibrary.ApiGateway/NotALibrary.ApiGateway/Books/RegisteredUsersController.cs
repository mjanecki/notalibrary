﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using NotALibrary.ApiGateway.Books.Dtos;
using NotALibrary.ApiGateway.Books.Services;
using NotALibrary.ApiGateway.Common;
using NotALibrary.Commands;

namespace NotALibrary.ApiGateway.Books
{
    public class RegisteredUsersController : BaseController
    {
        private readonly IBus _bus;
        private readonly IMapper _mapper;
        private readonly IRegisteredUsersService _registeredUsersService;

        public RegisteredUsersController(IBus bus,
            IMapper mapper,
            IRegisteredUsersService registeredUsersService)
        {
            _bus = bus;
            _mapper = mapper;
            _registeredUsersService = registeredUsersService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllRegisteredUsers()
            => ResultFromResponse(await _registeredUsersService.GetAllAsync());

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetRegisteredUser([FromRoute] Guid userId)
            => ResultFromResponse(await _registeredUsersService.GetByIdAsync(userId));

        [HttpPost]
        public Task<IActionResult> RegisterUser([FromBody] RegisterUserDto dto)
            => ProcessCommandAsync(_bus, _mapper.Map<RegisterUser>(dto));
    }
}
