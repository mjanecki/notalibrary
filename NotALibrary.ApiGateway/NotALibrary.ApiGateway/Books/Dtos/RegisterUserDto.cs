﻿namespace NotALibrary.ApiGateway.Books.Dtos
{
    public class RegisterUserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
