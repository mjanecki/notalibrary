﻿namespace NotALibrary.ApiGateway.Books.Dtos
{
    public class CreateBookDto
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}
