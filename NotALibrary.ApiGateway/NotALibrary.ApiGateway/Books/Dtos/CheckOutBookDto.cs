﻿using System;

namespace NotALibrary.ApiGateway.Books.Dtos
{
    public class CheckOutBookDto
    {
        public Guid UserId { get; set; }
        public DateTime ReturnDate { get; set; }
    }
}
