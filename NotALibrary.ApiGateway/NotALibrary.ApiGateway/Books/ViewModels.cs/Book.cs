﻿using System;
using System.Collections.Generic;

namespace NotALibrary.ApiGateway.Books.ViewModels.cs
{
    public class Book
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public List<BookItem> Items { get; set; }
    }
}
