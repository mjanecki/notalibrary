﻿namespace NotALibrary.ApiGateway.Books.ViewModels.cs.Enums
{
    public enum BookStatus
    {
        Available,
        Lent
    }
}
