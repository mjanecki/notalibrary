﻿using System;

namespace NotALibrary.ApiGateway.Books.ViewModels.cs
{
    public class RegisteredUser
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
