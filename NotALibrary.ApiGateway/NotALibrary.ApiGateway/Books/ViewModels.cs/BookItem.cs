﻿using System;
using NotALibrary.ApiGateway.Books.ViewModels.cs.Enums;

namespace NotALibrary.ApiGateway.Books.ViewModels.cs
{
    public class BookItem
    {
        public Guid Id { get; set; }
        public Guid? BorrowerId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public BookStatus Status { get; set; }
    }
}
