﻿using AutoMapper;
using NotALibrary.ApiGateway.Books.Dtos;
using NotALibrary.Commands;

namespace NotALibrary.ApiGateway.Books.Mappings
{
    public class RegisteredUsersProfile : Profile
    {
        public RegisteredUsersProfile()
        {
            CreateMap<RegisterUserDto, RegisterUser>();
        }
    }
}
