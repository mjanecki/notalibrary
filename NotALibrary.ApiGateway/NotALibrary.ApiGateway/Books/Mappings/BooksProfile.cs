﻿using AutoMapper;
using NotALibrary.ApiGateway.Books.Dtos;
using NotALibrary.Commands;

namespace NotALibrary.ApiGateway.Books.Mappings
{
    public class BooksProfile : Profile
    {
        public BooksProfile()
        {
            CreateMap<CreateBookDto, CreateBook>();
            CreateMap<CheckOutBookDto, CheckOutBook>();
        }
    }
}
