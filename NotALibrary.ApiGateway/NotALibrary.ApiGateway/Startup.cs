using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NotALibrary.ApiGateway.Books.Services;
using NotALibrary.Commons.Http.RestEase;
using NotALibrary.Commons.MassTransit;
using NotALibrary.Commons.Swagger;

namespace NotALibrary.ApiGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddRabbitMqWithMassTransit(Configuration);
            services.AddSwaggerDocs(Configuration);
            services.RegisterServiceForwarder<IBooksService>("books-service", Configuration);
            services.RegisterServiceForwarder<IRegisteredUsersService>("registered-users-service", Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseSwaggerDocs();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
