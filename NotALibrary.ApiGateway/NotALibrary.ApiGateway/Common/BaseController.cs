﻿using System;
using System.Net;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.ApiGateway.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected async Task<IActionResult> ProcessCommandAsync<T>(IBus bus, T command) where T : class, ICommand
        {
            command.CorrelationId = Guid.NewGuid();
            await bus.Send(command);
            return Accepted(command.CorrelationId);
        }

        protected IActionResult ResultFromResponse<T>(RestEase.Response<T> response)
        {
            if (response.ResponseMessage.StatusCode == HttpStatusCode.OK)
            {
                var model = typeof(T) == typeof(string) ? (T)(response.StringContent as object) : response.GetContent();
                return Ok(model);
            }

            if (response.ResponseMessage.StatusCode == HttpStatusCode.BadRequest)
            {
                return BadRequest(response.GetContent());
            }

            return StatusCode((int)response.ResponseMessage.StatusCode);
        }
    }
}
