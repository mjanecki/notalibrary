﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NotALibrary.Books.Domain.Models;
using NotALibrary.Books.Domain.Repositories;

namespace NotALibrary.Books.Infrastructure.Repositories
{
    public class RegisteredUsersInMemoryRepository : IRegisteredUsersRepository
    {
        private readonly List<RegisteredUser> _registeredUsers = new List<RegisteredUser>();

        public Task AddAsync(RegisteredUser user)
        {
            _registeredUsers.Add(user);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(RegisteredUser user)
        {
            var indexOfItemToUpdate = _registeredUsers.FindIndex(userToUpdate => userToUpdate.Id == user.Id);
            if (indexOfItemToUpdate >= 0)
            {
                _registeredUsers[indexOfItemToUpdate] = user;
            }

            return Task.CompletedTask;
        }

        public Task<RegisteredUser> GetByIdAsync(Guid id)
            => Task.FromResult(_registeredUsers.FirstOrDefault(user => user.Id == id));

        public Task<IEnumerable<RegisteredUser>> GetAllAsync()
            => Task.FromResult(_registeredUsers.AsEnumerable());
    }
}
