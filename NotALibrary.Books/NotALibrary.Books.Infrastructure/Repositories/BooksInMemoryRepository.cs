﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NotALibrary.Books.Domain.Models;
using NotALibrary.Books.Domain.Repositories;

namespace NotALibrary.Books.Infrastructure.Repositories
{

    public class BooksInMemoryRepository : IBookRepository
    {
        private readonly List<Book> _books = new List<Book>();

        public Task AddAsync(Book book)
        {
            _books.Add(book);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(Book book)
        {
            var indexOfItemToUpdate = _books.FindIndex(bookToUpdate => bookToUpdate.Id == book.Id);
            if (indexOfItemToUpdate >= 0)
            {
                _books[indexOfItemToUpdate] = book;
            }

            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            var entityToDelete = _books.FirstOrDefault(book => book.Id == id);
            _books.Remove(entityToDelete);

            return Task.CompletedTask;
        }

        public Task<Book> GetByIdAsync(Guid id)
            => Task.FromResult(_books.FirstOrDefault(book => book.Id == id));

        public Task<IEnumerable<Book>> GetAllAsync()
            => Task.FromResult(_books.AsEnumerable());
    }
}
