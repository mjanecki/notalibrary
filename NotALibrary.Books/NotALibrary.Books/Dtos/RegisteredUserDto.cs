﻿using System;

namespace NotALibrary.Books.Dtos
{
    public class RegisteredUserDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
