﻿using System;
using System.Collections.Generic;

namespace NotALibrary.Books.Dtos
{
    public class BookDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public List<BookItemDto> Items { get; set; }
    }
}
