﻿using System;
using NotALibrary.Books.Domain.Models.Enums;

namespace NotALibrary.Books.Dtos
{
    public class BookItemDto
    {
        public Guid Id { get; set; }
        public Guid? BorrowerId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public BookStatus Status { get; set; }
    }
}