using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NotALibrary.Books.Domain.Factories;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Books.Dtos;
using NotALibrary.Books.Infrastructure.Repositories;
using NotALibrary.Books.Queries;
using NotALibrary.Books.QueryHandlers;
using NotALibrary.Commons.Browsing.QueryDispatcher;
using NotALibrary.Commons.MassTransit;
using NotALibrary.Commons.Swagger;

namespace NotALibrary.Books
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddRabbitMqWithMassTransit(Configuration);
            services.AddSwaggerDocs(Configuration);
            services.AddQueryDispatcher();

            services.AddTransient<IBookFactory, BookFactory>();
            services.AddTransient<IRegisteredUserFactory, RegisteredUserFactory>();
            services.AddSingleton<IBookRepository, BooksInMemoryRepository>();
            services.AddSingleton<IRegisteredUsersRepository, RegisteredUsersInMemoryRepository>();
            services.AddTransient<IQueryHandler<BrowseBooks, IEnumerable<BookDto>>, BrowseBooksHandler>();
            services.AddTransient<IQueryHandler<GetBook, BookDto>, GetBookHandler>();
            services.AddTransient<IQueryHandler<BrowseRegisteredUsers, IEnumerable<RegisteredUserDto>>, BrowseRegisteredUsersHandler>();
            services.AddTransient<IQueryHandler<GetRegisteredUser, RegisteredUserDto>, GetRegisteredUserHandler>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseSwaggerDocs();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
