﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotALibrary.Commons.Browsing.QueryDispatcher;
using NotALibrary.Commons.Browsing.Types;

namespace NotALibrary.Books.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;

        public BaseController(IQueryDispatcher queryDispatcher)
        {
            _queryDispatcher = queryDispatcher;
        }

        protected async Task<TResult> QueryAsync<TResult>(IQuery<TResult> query)
            => await _queryDispatcher.QueryAsync(query);

        protected ActionResult<T> Result<T>(T data)
        {
            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        }
    }
}
