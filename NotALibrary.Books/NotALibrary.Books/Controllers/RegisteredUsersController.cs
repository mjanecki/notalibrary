﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotALibrary.Books.Dtos;
using NotALibrary.Books.Queries;
using NotALibrary.Commons.Browsing.QueryDispatcher;

namespace NotALibrary.Books.Controllers
{
    public class RegisteredUsersController : BaseController
    {
        public RegisteredUsersController(IQueryDispatcher queryDispatcher) : base(queryDispatcher)
        {
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<RegisteredUserDto>> GetRegisteredUser([FromRoute] GetRegisteredUser query)
            => Result(await QueryAsync(query));

        [HttpGet]
        public async Task<ActionResult<IEnumerable<RegisteredUserDto>>> BrowseRegisteredUsers()
            => Result(await QueryAsync(new BrowseRegisteredUsers()));
    }
}
