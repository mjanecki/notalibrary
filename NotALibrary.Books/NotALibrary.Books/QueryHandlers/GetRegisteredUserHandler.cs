﻿using System.Threading.Tasks;
using AutoMapper;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Books.Dtos;
using NotALibrary.Books.Queries;
using NotALibrary.Commons.Browsing.QueryDispatcher;

namespace NotALibrary.Books.QueryHandlers
{
    public class GetRegisteredUserHandler : IQueryHandler<GetRegisteredUser, RegisteredUserDto>
    {
        private readonly IRegisteredUsersRepository _registeredUsersRepository;
        private readonly IMapper _mapper;

        public GetRegisteredUserHandler(IRegisteredUsersRepository registeredUsersRepository,
            IMapper mapper)
        {
            _registeredUsersRepository = registeredUsersRepository;
            _mapper = mapper;
        }

        public async Task<RegisteredUserDto> HandleAsync(GetRegisteredUser query)
        {
            var user = await _registeredUsersRepository.GetByIdAsync(query.Id);
            return user == null ? null : _mapper.Map<RegisteredUserDto>(user);
        }
    }
}
