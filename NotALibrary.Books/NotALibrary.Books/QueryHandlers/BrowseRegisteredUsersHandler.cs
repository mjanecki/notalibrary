﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Books.Dtos;
using NotALibrary.Books.Queries;
using NotALibrary.Commons.Browsing.QueryDispatcher;

namespace NotALibrary.Books.QueryHandlers
{

    public class BrowseRegisteredUsersHandler : IQueryHandler<BrowseRegisteredUsers, IEnumerable<RegisteredUserDto>>
    {
        private readonly IRegisteredUsersRepository _registeredUsersRepository;
        private readonly IMapper _mapper;

        public BrowseRegisteredUsersHandler(IRegisteredUsersRepository registeredUsersRepository,
            IMapper mapper)
        {
            _registeredUsersRepository = registeredUsersRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<RegisteredUserDto>> HandleAsync(BrowseRegisteredUsers query)
        {
            var users = await _registeredUsersRepository.GetAllAsync();
            return users?.Select(_mapper.Map<RegisteredUserDto>);
        }
    }
}
