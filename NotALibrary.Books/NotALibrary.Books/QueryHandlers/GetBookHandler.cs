﻿using System.Threading.Tasks;
using AutoMapper;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Books.Dtos;
using NotALibrary.Books.Queries;
using NotALibrary.Commons.Browsing.QueryDispatcher;

namespace NotALibrary.Books.QueryHandlers
{
    public class GetBookHandler : IQueryHandler<GetBook, BookDto>
    {
        private readonly IBookRepository _bookRepository;
        private readonly IMapper _mapper;

        public GetBookHandler(IBookRepository bookRepository,
            IMapper mapper)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public async Task<BookDto> HandleAsync(GetBook query)
        {
            var book = await _bookRepository.GetByIdAsync(query.Id);
            return book == null ? null : _mapper.Map<BookDto>(book);
        }
    }
}
