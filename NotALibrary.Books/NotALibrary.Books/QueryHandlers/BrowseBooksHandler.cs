﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Books.Dtos;
using NotALibrary.Books.Queries;
using NotALibrary.Commons.Browsing.QueryDispatcher;

namespace NotALibrary.Books.QueryHandlers
{
    public class BrowseBooksHandler : IQueryHandler<BrowseBooks, IEnumerable<BookDto>>
    {
        private readonly IBookRepository _bookRepository;
        private readonly IMapper _mapper;

        public BrowseBooksHandler(IBookRepository bookRepository,
            IMapper mapper)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<BookDto>> HandleAsync(BrowseBooks query)
        {
            var books = await _bookRepository.GetAllAsync();
            return books?.Select(_mapper.Map<BookDto>);
        }
    }
}
