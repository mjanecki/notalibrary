﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MassTransit;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Commands;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Books.Consumers
{
    public class CheckOutBookConsumer : CommandConsumerBase<CheckOutBook>
    {
        private readonly IBookRepository _bookRepository;
        private readonly IRegisteredUsersRepository _registeredUsersRepository;

        public CheckOutBookConsumer(IBookRepository bookRepository,
            IRegisteredUsersRepository registeredUsersRepository)
        {
            _bookRepository = bookRepository;
            _registeredUsersRepository = registeredUsersRepository;
        }

        public override async Task ProcessCommandAsync(CheckOutBook command, ConsumeContext<CheckOutBook> context)
        {
            var book = await _bookRepository.GetByIdAsync(command.BookId);
            if (book == null)
            {
                throw new KeyNotFoundException();
            }

            var user = await _registeredUsersRepository.GetByIdAsync(command.UserId);
            if (user == null)
            {
                throw new KeyNotFoundException();
            }

            if (!user.TryAssignBook(command.BookId))
            {
                throw new InvalidOperationException();
            };

            book.CheckOutBookItem(command.ItemId, command.UserId, command.ReturnDate);
            await _bookRepository.UpdateAsync(book);
            await _registeredUsersRepository.UpdateAsync(user);
        }
    }
}
