﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MassTransit;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Commands;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Books.Consumers
{
    public class AddBookItemsConsumer : CommandConsumerBase<AddBookItems>
    {
        private readonly IBookRepository _bookRepository;

        public AddBookItemsConsumer(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public override async Task ProcessCommandAsync(AddBookItems command, ConsumeContext<AddBookItems> context)
        {
            var book = await _bookRepository.GetByIdAsync(command.BookId);

            if (book == null)
            {
                throw new KeyNotFoundException();
            }

            book.AddBookItems(command.Quantity);
            await _bookRepository.UpdateAsync(book);
        }
    }
}
