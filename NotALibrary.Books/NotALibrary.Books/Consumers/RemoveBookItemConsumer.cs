﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MassTransit;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Commands;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Books.Consumers
{
    public class RemoveBookItemConsumer : CommandConsumerBase<RemoveBookItem>
    {
        private readonly IBookRepository _bookRepository;

        public RemoveBookItemConsumer(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public override async Task ProcessCommandAsync(RemoveBookItem command, ConsumeContext<RemoveBookItem> context)
        {
            var book = await _bookRepository.GetByIdAsync(command.BookId);

            if (book == null)
            {
                throw new KeyNotFoundException();
            }

            book.RemoveBookItem(command.ItemId);
            await _bookRepository.UpdateAsync(book);
        }
    }
}
