﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Commands;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Books.Consumers
{
    public class CheckInBookConsumer : CommandConsumerBase<CheckInBook>
    {
        private readonly IBookRepository _bookRepository;
        private readonly IRegisteredUsersRepository _registeredUsersRepository;

        public CheckInBookConsumer(IBookRepository bookRepository, 
            IRegisteredUsersRepository registeredUsersRepository)
        {
            _bookRepository = bookRepository;
            _registeredUsersRepository = registeredUsersRepository;
        }

        public override async Task ProcessCommandAsync(CheckInBook command, ConsumeContext<CheckInBook> context)
        {
            var book = await _bookRepository.GetByIdAsync(command.BookId);
            if (book == null)
            {
                throw new KeyNotFoundException();
            }

            var user = await _registeredUsersRepository.GetByIdAsync(book.Items.FirstOrDefault(item => item.Id == command.ItemId)?.BorrowerId ?? Guid.Empty);
            if (user == null)
            {
                throw new KeyNotFoundException();
            }

            user.TakeBackBook(book.Id);
            book.CheckInBookItem(command.ItemId);
            await _bookRepository.UpdateAsync(book);
            await _registeredUsersRepository.UpdateAsync(user);
        }
    }
}
