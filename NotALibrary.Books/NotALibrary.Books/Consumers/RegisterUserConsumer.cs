﻿using System.Threading.Tasks;
using MassTransit;
using NotALibrary.Books.Domain.Factories;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Commands;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Books.Consumers
{
    public class RegisterUserConsumer : CommandConsumerBase<RegisterUser>
    {
        private readonly IRegisteredUsersRepository _registeredUsersRepository;
        private readonly IRegisteredUserFactory _registeredUserFactory;

        public RegisterUserConsumer(IRegisteredUsersRepository registeredUsersRepository,
            IRegisteredUserFactory registeredUserFactory)
        {
            _registeredUsersRepository = registeredUsersRepository;
            _registeredUserFactory = registeredUserFactory;
        }

        public override async Task ProcessCommandAsync(RegisterUser command, ConsumeContext<RegisterUser> context)
        {
            var registeredUser = _registeredUserFactory.Create(command.FirstName, command.LastName);
            await _registeredUsersRepository.AddAsync(registeredUser);
        }
    }
}
