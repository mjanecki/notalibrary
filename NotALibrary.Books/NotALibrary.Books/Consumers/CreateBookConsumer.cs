﻿using System.Threading.Tasks;
using MassTransit;
using NotALibrary.Books.Domain.Factories;
using NotALibrary.Books.Domain.Repositories;
using NotALibrary.Commands;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Books.Consumers
{
    public class CreateBookConsumer : CommandConsumerBase<CreateBook>
    {
        private readonly IBookRepository _bookRepository;
        private readonly IBookFactory _bookFactory;

        public CreateBookConsumer(IBookRepository bookRepository,
            IBookFactory bookFactory)
        {
            _bookRepository = bookRepository;
            _bookFactory = bookFactory;
        }

        public override async Task ProcessCommandAsync(CreateBook command, ConsumeContext<CreateBook> context)
        {
            var book = _bookFactory.Create(command.Title, command.Author);
            await _bookRepository.AddAsync(book);
        }
    }
}
