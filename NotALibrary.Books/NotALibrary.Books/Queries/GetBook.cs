﻿using System;
using NotALibrary.Books.Dtos;
using NotALibrary.Commons.Browsing.Types;

namespace NotALibrary.Books.Queries
{
    public class GetBook : IQuery<BookDto>
    {
        public Guid Id { get; set; }
    }
}
