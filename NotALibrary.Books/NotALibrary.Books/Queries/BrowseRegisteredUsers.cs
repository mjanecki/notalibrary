﻿using System.Collections.Generic;
using NotALibrary.Books.Dtos;
using NotALibrary.Commons.Browsing.Types;

namespace NotALibrary.Books.Queries
{
    public class BrowseRegisteredUsers : IQuery<IEnumerable<RegisteredUserDto>>
    {
    }
}
