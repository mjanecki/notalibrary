﻿using System;
using NotALibrary.Books.Dtos;
using NotALibrary.Commons.Browsing.Types;

namespace NotALibrary.Books.Queries
{
    public class GetRegisteredUser : IQuery<RegisteredUserDto>
    {
        public Guid Id { get; set; }
    }
}
