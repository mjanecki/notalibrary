﻿using AutoMapper;
using NotALibrary.Books.Domain.Models;
using NotALibrary.Books.Dtos;

namespace NotALibrary.Books.Mappings
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookDto>();
            CreateMap<BookItem, BookItemDto>();
        }
    }
}
