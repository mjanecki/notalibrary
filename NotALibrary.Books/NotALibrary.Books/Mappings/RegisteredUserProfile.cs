﻿using AutoMapper;
using NotALibrary.Books.Domain.Models;
using NotALibrary.Books.Dtos;

namespace NotALibrary.Books.Mappings
{
    public class RegisteredUserProfile : Profile
    {
        public RegisteredUserProfile()
        {
            CreateMap<RegisteredUser, RegisteredUserDto>();
        }
    }
}
