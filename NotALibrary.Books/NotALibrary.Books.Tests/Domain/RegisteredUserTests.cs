﻿using System;
using NotALibrary.Books.Domain.Models;
using NUnit.Framework;
using Shouldly;

namespace NotALibrary.Books.Tests.Domain
{
    [TestFixture]
    public class RegisteredUserTests
    {
        public void TryAssignBook_should_return_false_if_user_already_has_this_book()
        {
            var bookId = Guid.NewGuid();
            var user = new RegisteredUser("Test", "Test");
            user.BorrowedBookIds.Add(bookId);

            var result = user.TryAssignBook(bookId);

            result.ShouldBe(false);
        }

        public void TryAssignBook_should_return_false_if_user_already_has_4_other_books()
        {
            var user = new RegisteredUser("Test", "Test");
            for (var i = 0; i < 4; i++)
            {
                user.BorrowedBookIds.Add(Guid.NewGuid());
            }

            var result = user.TryAssignBook(Guid.NewGuid());

            result.ShouldBe(false);
        }

        public void TryAssignBook_should_return_true_if_user_can_borrow_book()
        {
            var user = new RegisteredUser("Test", "Test");

            var result = user.TryAssignBook(Guid.NewGuid());

            result.ShouldBe(true);
        }
    }
}
