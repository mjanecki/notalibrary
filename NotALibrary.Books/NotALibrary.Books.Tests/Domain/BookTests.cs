using System;
using NotALibrary.Books.Domain.Models;
using NotALibrary.Books.Domain.Models.Enums;
using NUnit.Framework;
using Shouldly;

namespace NotALibrary.Books.Tests.Domain
{
    [TestFixture]
    public class BookTests
    {
        [TestCase(2)]
        [TestCase(10)]
        [TestCase(1)]
        public void AddBookItems_should_add_available_items(int quantity)
        {
            var book = new Book("test", "test");

            book.AddBookItems(quantity);

            book.Items.Count.ShouldBe(quantity);
            book.Items.ShouldAllBe(item => item.Status == BookStatus.Available);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-10)]
        public void AddBookItems_should_throw_exception_if_quantity_is_invalid(int quantity)
        {
            var book = new Book("test", "test");

            Should.Throw<ArgumentException>(() => book.AddBookItems(quantity));

            book.Items.Count.ShouldBe(0);
        }
    }
}