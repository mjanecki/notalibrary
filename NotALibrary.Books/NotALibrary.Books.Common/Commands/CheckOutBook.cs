﻿using System;
using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Commands
{
    public class CheckOutBook : CommandBase
    {
        public Guid BookId { get; set; }
        public Guid ItemId { get; set; }
        public Guid UserId { get; set; }
        public DateTime ReturnDate { get; set; }

        public CheckOutBook WithBookId(Guid bookId)
        {
            BookId = bookId;
            return this;
        }
        public CheckOutBook WithItemId(Guid itemId)
        {
            ItemId = itemId;
            return this;
        }
    }
}