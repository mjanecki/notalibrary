﻿using NotALibrary.Commons.MassTransit;

namespace NotALibrary.Commands
{
    public class CreateBook : CommandBase
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}
