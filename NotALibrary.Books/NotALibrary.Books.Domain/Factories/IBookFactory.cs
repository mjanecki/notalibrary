﻿using NotALibrary.Books.Domain.Models;

namespace NotALibrary.Books.Domain.Factories
{
    public interface IBookFactory
    {
        Book Create(string title, string author);
    }
}
