﻿using NotALibrary.Books.Domain.Models;

namespace NotALibrary.Books.Domain.Factories
{
    public interface IRegisteredUserFactory
    {
        RegisteredUser Create(string firstName, string lastName);
    }
}
