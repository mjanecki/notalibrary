﻿using NotALibrary.Books.Domain.Models;

namespace NotALibrary.Books.Domain.Factories
{
    public class RegisteredUserFactory : IRegisteredUserFactory
    {
        public RegisteredUser Create(string firstName, string lastName)
            => new RegisteredUser(firstName, lastName);
    }
}