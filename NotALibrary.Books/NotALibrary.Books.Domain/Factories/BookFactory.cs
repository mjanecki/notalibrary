﻿using NotALibrary.Books.Domain.Models;

namespace NotALibrary.Books.Domain.Factories
{
    public class BookFactory : IBookFactory
    {
        public Book Create(string title, string author)
            => new Book(title, author);
    }
}