﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NotALibrary.Books.Domain.Models;

namespace NotALibrary.Books.Domain.Repositories
{
    public interface IBookRepository
    {
        Task AddAsync(Book book);

        Task UpdateAsync(Book book);

        Task DeleteAsync(Guid id);

        Task<Book> GetByIdAsync(Guid id);

        Task<IEnumerable<Book>> GetAllAsync();
    }
}
