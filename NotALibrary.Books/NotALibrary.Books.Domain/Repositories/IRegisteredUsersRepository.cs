﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NotALibrary.Books.Domain.Models;

namespace NotALibrary.Books.Domain.Repositories
{
    public interface IRegisteredUsersRepository
    {
        Task AddAsync(RegisteredUser user);

        Task UpdateAsync(RegisteredUser user);

        Task<RegisteredUser> GetByIdAsync(Guid id);

        Task<IEnumerable<RegisteredUser>> GetAllAsync();
    }
}
