﻿using System;
using System.Collections.Generic;
using System.Linq;
using NotALibrary.Books.Domain.Models.Enums;

namespace NotALibrary.Books.Domain.Models
{
    public class Book
    {
        public Guid Id { get; }
        public string Title { get; }
        public string Author { get; }
        public List<BookItem> Items { get; }

        internal Book(string title, string author)
        {
            Id = Guid.NewGuid();
            Title = title;
            Author = author;
            Items = new List<BookItem>();
        }

        public void AddBookItems(int quantity)
        {
            if (quantity < 1)
            {
                throw new ArgumentException("Quantity has to be bigger than 0");
            }

            Items.AddRange(Enumerable.Range(1, quantity).Select(_ => BookItem.New));
        }

        public void RemoveBookItem(Guid id)
        {
            var itemToRemove = Items.FirstOrDefault(item => item.Id == id);
            if (itemToRemove != null)
            {
                Items.Remove(itemToRemove);
            }
        }

        public void CheckOutBookItem(Guid itemId, Guid borrowerId, DateTime returnDate)
        {
            var itemToCheckOut = Items.FirstOrDefault(item => item.Id == itemId && item.Status == BookStatus.Available);

            if (itemToCheckOut == null)
            {
                throw new InvalidOperationException("Book item not found");
            }

            itemToCheckOut.CheckOut(borrowerId, returnDate);
        }

        public void CheckInBookItem(Guid itemId)
        {
            var itemToCheckIn = Items.FirstOrDefault(item => item.Id == itemId && item.Status == BookStatus.Lent);

            if (itemToCheckIn == null)
            {
                throw new InvalidOperationException("Book item not found");
            }

            itemToCheckIn.CheckIn();
        }
    }
}
