﻿namespace NotALibrary.Books.Domain.Models.Enums
{
    public enum BookStatus
    {
        Available,
        Lent
    }
}
