﻿using System;
using NotALibrary.Books.Domain.Models.Enums;

namespace NotALibrary.Books.Domain.Models
{
    public class BookItem
    {
        public Guid Id { get; }
        public Guid? BorrowerId { get; private set; }
        public DateTime? ReturnDate { get; private set; }
        public BookStatus Status { get; private set; }

        private BookItem()
        {
            Id = Guid.NewGuid();
            Status = BookStatus.Available;
        }

        public static BookItem New => new BookItem();

        public void CheckOut(Guid borrowerId, DateTime returnDate)
        {
            if (Status != BookStatus.Available)
            {
                throw new InvalidOperationException("Book is not available");
            }

            BorrowerId = borrowerId;
            ReturnDate = returnDate;
            Status = BookStatus.Lent;
        }

        public void CheckIn()
        {
            if (Status != BookStatus.Lent)
            {
                throw new InvalidOperationException("Book is not lent");
            }

            BorrowerId = null;
            ReturnDate = null;
            Status = BookStatus.Available;
        }
    }
}
