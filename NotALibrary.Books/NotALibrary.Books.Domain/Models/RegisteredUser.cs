﻿using System;
using System.Collections.Generic;

namespace NotALibrary.Books.Domain.Models
{
    public class RegisteredUser
    {
        public Guid Id { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public ICollection<Guid> BorrowedBookIds { get; } = new List<Guid>();

        private const int BookLimit = 4;

        internal RegisteredUser(string firstName, string lastName)
        {
            Id = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
        }

        public bool TryAssignBook(Guid bookId)
        {
            if (BorrowedBookIds.Count > BookLimit)
            {
                return false;
            }

            if (BorrowedBookIds.Contains(bookId))
            {
                return false;
            }

            BorrowedBookIds.Add(bookId);
            return true;
        }

        public void TakeBackBook(Guid bookId)
        {
            if (BorrowedBookIds.Contains(bookId))
            {
                BorrowedBookIds.Remove(bookId);
            }
        }
    }
}
