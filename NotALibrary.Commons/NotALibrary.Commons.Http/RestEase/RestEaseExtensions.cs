﻿using System;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NotALibrary.Commons.Core;
using RestEase;

namespace NotALibrary.Commons.Http.RestEase
{
    public static class RestEaseExtensions
    {
        public static void RegisterServiceForwarder<T>(this IServiceCollection services, string serviceName,
            IConfiguration configuration) where T : class
        {
            var clientName = typeof(T).ToString();
            var options = ConfigureOptions(services, configuration);
            switch (options.LoadBalancer?.ToLowerInvariant())
            {
                default:
                    ConfigureDefaultClient(services, clientName, serviceName, options);
                    break;
            }

            ConfigureForwarder<T>(services, clientName);
        }

        private static void ConfigureForwarder<T>(IServiceCollection services, string clientName) where T : class
        {
            services.AddTransient(c =>
                new RestClient(c.GetService<System.Net.Http.IHttpClientFactory>().CreateClient(clientName))
                {
                    RequestQueryParamSerializer = new QueryParamSerializer()
                }.For<T>());
        }

        private static void ConfigureDefaultClient(IServiceCollection services, string clientName, string serviceName,
            RestEaseOptions options)
        {
            services.AddHttpClient(clientName, client =>
            {
                var service = options.Services.SingleOrDefault(s =>
                    s.Name.Equals(serviceName, StringComparison.InvariantCultureIgnoreCase));
                if (service == null)
                    throw new RestEaseServiceNotFoundException($"RestEase service: '{serviceName}' was not found.",
                        serviceName);

                client.BaseAddress = new UriBuilder {Scheme = service.Scheme, Host = service.Host, Port = service.Port}
                    .Uri;
            });
        }

        private static RestEaseOptions ConfigureOptions(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RestEaseOptions>(configuration.GetSection("restEase"));
            return configuration.GetOptions<RestEaseOptions>("restEase");
        }
    }
}
