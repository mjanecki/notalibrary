﻿namespace NotALibrary.Commons.MassTransit
{
    public class AppOptions
    {
        public string Name { get; set; }
        public string RootNamespace { get; set; }
    }
}
