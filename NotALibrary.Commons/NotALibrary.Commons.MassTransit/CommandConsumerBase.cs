﻿using System;
using System.Threading.Tasks;
using MassTransit;

namespace NotALibrary.Commons.MassTransit
{
    public abstract class CommandConsumerBase<T> : IConsumer<T> where T : class, ICommand
    {
        public async Task Consume(ConsumeContext<T> context)
        {
            try
            {
                await ProcessCommandAsync(context.Message, context);
            }
            catch (Exception)
            {
                // Log
            }
        }

        public abstract Task ProcessCommandAsync(T command, ConsumeContext<T> context);
    }
}