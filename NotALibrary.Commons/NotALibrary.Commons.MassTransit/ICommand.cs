﻿using System;

namespace NotALibrary.Commons.MassTransit
{
    public interface ICommand
    {
        Guid CorrelationId { get; set; }
    }
}
