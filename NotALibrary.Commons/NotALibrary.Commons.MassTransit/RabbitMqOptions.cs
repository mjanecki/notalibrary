﻿namespace NotALibrary.Commons.MassTransit
{
    public class RabbitMqOptions
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string VirtualHost { get; set; }
        public string Host { get; set; }
    }
}
