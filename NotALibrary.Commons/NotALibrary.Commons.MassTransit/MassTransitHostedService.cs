﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Hosting;

namespace NotALibrary.Commons.MassTransit
{
    public sealed class MassTransitHostedService : IHostedService
    {
        private readonly IBusControl _busControl;

        public MassTransitHostedService(IBusControl busControl)
        {
            _busControl = busControl;
        }

        public Task StartAsync(CancellationToken cancellationToken)
            => _busControl.StartAsync(cancellationToken);

        public  Task StopAsync(CancellationToken cancellationToken)
            => _busControl.StopAsync(TimeSpan.FromSeconds(10));
    }
}
