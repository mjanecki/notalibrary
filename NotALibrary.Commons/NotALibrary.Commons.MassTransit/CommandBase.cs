﻿using System;

namespace NotALibrary.Commons.MassTransit
{
    public class CommandBase : ICommand
    {
        public Guid CorrelationId { get; set; }
    }
}
