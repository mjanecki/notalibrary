﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GreenPipes;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NotALibrary.Commons.Core;

namespace NotALibrary.Commons.MassTransit
{
    public static class MassTransitExtensions
    {
        public static IServiceCollection AddRabbitMqWithMassTransit(this IServiceCollection services,
            IConfiguration configuration)
        {
            var rabbitOptions = configuration.GetOptions<RabbitMqOptions>("rabbitMq");
            var appOptions = configuration.GetOptions<AppOptions>("app");
            ConfigureRabbitMq(services, rabbitOptions, appOptions);
            return services;
        }

        private static void ConfigureRabbitMq(IServiceCollection services, RabbitMqOptions rabbitOptions,
            AppOptions appOptions)
        {
            var assemblies = GetProjectAssemblies(appOptions.RootNamespace);
            var commands = assemblies.ListTypesFromAssemblies<ICommand>();
            var consumers = assemblies.ListTypesFromAssemblies<IConsumer>()
                .Except(new List<Type>
                {
                    typeof(CommandConsumerBase<>),
                })
                .ToList();

            foreach (var consumer in consumers)
            {
                services.AddScoped(consumer);
            }

            services.AddMassTransit(configurator =>
                {
                    foreach (var consumer in consumers)
                    {
                        configurator.AddConsumer(consumer);
                    }

                    configurator.AddBus(provider =>
                    {
                        var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                        {
                            var rabbitHost = cfg.Host(rabbitOptions.Host, rabbitOptions.VirtualHost,
                                hostConfigurator =>
                                {
                                    hostConfigurator.Username(rabbitOptions.Username);
                                    hostConfigurator.Password(rabbitOptions.Password);
                                });

                            foreach (var consumer in consumers)
                            {
                                var consumerType = GetCommandTypeFromConsumerClass(consumer);

                                if (typeof(ICommand).IsAssignableFrom(consumerType))
                                {
                                    cfg.ReceiveEndpoint(rabbitHost, GetQueueNameForType(consumerType), ep =>
                                    {
                                        ep.PrefetchCount = 16;
                                        ep.UseMessageRetry(x => x.Interval(2, 100));
                                        ep.ConfigureConsumer(provider, consumer);
                                    });
                                }
                            }
                        });

                        return busControl;
                    });

                    foreach (var command in commands)
                    {
                        var method = typeof(EndpointConvention).GetMethod("Map", new[] {typeof(Uri)});
                        if (method != null)
                        {
                            method.MakeGenericMethod(command).Invoke(null,
                                new object[]
                                {
                                    new Uri($"rabbitmq://{rabbitOptions.Host}{rabbitOptions.VirtualHost}{GetQueueNameForType(command)}")
                                });
                        }
                    }
                }
            );

            services.AddSingleton<IHostedService, MassTransitHostedService>();
        }

        private static Type GetCommandTypeFromConsumerClass(Type type)
            => type.GetInterfaces()
                .FirstOrDefault(x => x.IsGenericType)?
                .GetGenericArguments()
                .FirstOrDefault();

        private static string GetQueueNameForType(Type type)
            => $"{type.Name.ToLower()}_queue";

        private static List<Assembly> GetProjectAssemblies(string applicationRootNamespace)
        {
            var entryAssembly = Assembly.GetEntryAssembly() ?? throw new ApplicationException();
            var refAssembliesNames = entryAssembly.GetReferencedAssemblies()
                .Where(name => name.Name.StartsWith(applicationRootNamespace));

            var result = new List<Assembly> { entryAssembly };
            result.AddRange(refAssembliesNames.Select(Assembly.Load));

            return result;
        }

        private static List<Type> ListTypesFromAssemblies<T>(this List<Assembly> list)
        {
            var result = new List<Type>();

            foreach (var item in list)
            {
                result.AddRange(item.GetTypes().Where(x => x.IsClass && typeof(T).IsAssignableFrom(x)).AsEnumerable());
            }

            return result;
        }
    }
}
