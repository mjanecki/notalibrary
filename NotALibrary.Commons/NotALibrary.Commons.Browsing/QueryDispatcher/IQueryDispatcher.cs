﻿using System.Threading.Tasks;
using NotALibrary.Commons.Browsing.Types;

namespace NotALibrary.Commons.Browsing.QueryDispatcher
{
    public interface IQueryDispatcher
    {
        Task<TResult> QueryAsync<TResult>(IQuery<TResult> query);
    }
}
