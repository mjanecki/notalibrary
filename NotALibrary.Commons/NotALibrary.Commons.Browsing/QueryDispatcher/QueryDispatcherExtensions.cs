﻿using Microsoft.Extensions.DependencyInjection;

namespace NotALibrary.Commons.Browsing.QueryDispatcher
{
    public static class QueryDispatcherExtensions
    {
        public static IServiceCollection AddQueryDispatcher(this IServiceCollection services)
        {
            services.AddTransient<IQueryDispatcher, QueryDispatcher>();
            return services;
        }
    }
}
