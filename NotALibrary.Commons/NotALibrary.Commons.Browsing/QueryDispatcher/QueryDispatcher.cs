﻿using System;
using System.Threading.Tasks;
using NotALibrary.Commons.Browsing.Types;

namespace NotALibrary.Commons.Browsing.QueryDispatcher
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public QueryDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TResult> QueryAsync<TResult>(IQuery<TResult> query)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));

            dynamic handler = _serviceProvider.GetService(handlerType);

            if (handler == null)
            {
                throw new InvalidOperationException($"Handler not registered for {query.GetType().FullName}");
            }

            return await handler.HandleAsync((dynamic)query);
        }
    }
}
