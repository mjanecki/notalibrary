﻿using System.Threading.Tasks;
using NotALibrary.Commons.Browsing.Types;

namespace NotALibrary.Commons.Browsing.QueryDispatcher
{
    public interface IQueryHandler<in TQuery, TResult> where TQuery : IQuery<TResult>
    {
        Task<TResult> HandleAsync(TQuery query);
    }
}
