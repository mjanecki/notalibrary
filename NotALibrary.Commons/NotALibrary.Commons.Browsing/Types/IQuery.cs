﻿namespace NotALibrary.Commons.Browsing.Types
{
    public interface IQuery
    {
    }

    public interface IQuery<T> : IQuery
    {
    }
}
